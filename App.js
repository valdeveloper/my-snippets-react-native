import { StatusBar } from "expo-status-bar"
import React from "react"
import { StyleSheet, View } from "react-native"
import Login from "./views/Login"

export default function App() {
    return (
        <View style={styles.container}>
            <Login/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#333",
        alignItems: "stretch",
        justifyContent: "center",
        padding: 15,
    },
})
