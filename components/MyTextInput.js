import React, { useState } from "react"
import { StyleSheet, Text, View, TextInput } from "react-native"

const MyTextInput = ({ label, theState, setTheState, secureTextEntry = false, returnKeyType }) => {


    return (
        <View>
            <Text style={styles.label}>{label}</Text>
            <TextInput
                style={styles.input}
                onChangeText={setTheState}
                value={theState}
                secureTextEntry={secureTextEntry}
                returnKeyType={returnKeyType}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    label: {
        color: "white",
        marginBottom: 6,
        marginLeft: 4,
        fontSize: 18,
        fontWeight: "bold",
    },
    input: {
        height: 50,
        borderWidth: 1,
        borderColor: "white",
        borderRadius: 10,
        color: "white",
        padding: 10,
        fontSize: 18,
        marginBottom: 10,
    },
})

export default MyTextInput
