import React from "react"
import { StyleSheet, Text } from "react-native"

const CustomText = ({ children, callback }) => {
    return (
        <Text style={styles.label} onPress={() => callback()}>
            {children}
        </Text>
    )
}

const styles = StyleSheet.create({
    label: {
        color: "#79a6fe",
        marginBottom: 6,
        fontSize: 18,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: 20,
    },
})

export default CustomText
