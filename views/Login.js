import React, { useState } from "react"
import { StyleSheet, View, Alert } from "react-native"
import MyTextInput from "../components/MyTextInput"
import CustomButton from "../components/CustomButton"
import CustomText from "../components/CustomText"

const Login = ({ children }) => {
    const [login, setLogin] = useState("")
    const [password, setPassword] = useState("")

    const handleSubmit = () => {
        Alert.alert("Alert Title", "My Alert Msg", [
            {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
            },
            { text: "OK", onPress: () => console.log("OK Pressed") },
        ])
    }

    return (
        <>
            <MyTextInput
                label="Login"
                theState={login}
                setTheState={setLogin}
                returnKeyType="next"
            />
            <MyTextInput
                label="Password"
                theState={password}
                setTheState={setPassword}
                secureTextEntry={true}
                returnKeyType="done"
            />
            <CustomButton label="LOGIN" callback={handleSubmit} />
            <CustomText callback={handleSubmit}>Already have an account ?</CustomText>
        </>
    )
}

export default Login
